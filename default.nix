{ pkgs ? import <nixpkgs> {} }:

pkgs.python3Packages.buildPythonApplication {

  pname = "icalepcs21";
  src = ./.;
  version = "0.0.1";

  propagatedBuildInputs = [ pkgs.python3Packages.flask ];

}
