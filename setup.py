from setuptools import setup

setup(

    name='icalepcs21',
    version='0.0.1',
    py_modules=['icalepcs21'],
    entry_points={
        'console_scripts': ['icalepcs21 = icalepcs21:run']
    },

)
